
		 <?php
                
                
		 	require_once 'header.php';
                        
                         
		 ?>

	 </head>
	<body data-spy="scroll" data-offset="50" data-twttr-rendered="true">
		
		<div class="container grid-bg" style="margin-top: 50px;">
			
			<div class="modal fade" id="progressModal">
				<div class="modal-header">
					<button class="close" data-dismiss="modal">×</button>
					<h3>Uploading...</h3>
				</div>
				<div class="modal-body">
					<div id="importProgress" class="progress progress-striped progress-success active">
						<div class="bar" style="width:100%"></div>
					</div>
					<div id="resultDiv"></div>
				</div>
				<div id="outputDiv" class="modal-footer">
					Depending on the size of your input, this might take a while.
				</div>
			</div>
			
			
			
			<div class="span4">
				<form class="form-horizontal" id="excelImportForm" action="login" method="post" enctype="multipart/form-data">
				  	<fieldset>
				  		<legend><center>Please Login with your Credentials<br><?php echo $message;?></center></legend>
				    	
				    	
				  		
				  		
				  		<div class="control-group">
				  			<label class="control-label" for="adm4heading">Username</label>
				  			<div class="controls">
				  				<input class="input-xlarge" required="required" type="text" id="adm4heading" 
				  						placeholder= 'Enter your username' name="name">
				  			</div>
				  		</div>
				  		
				  		
				  		<div class="control-group">
				  			<label class="control-label" for="longitudeheading">Password</label>
				  			<div class="controls">
                                                            <input class="input-xlarge" required="required" type="password" id="longitudeheading" 
				  						placeholder= 'Enter your password' name="password">
				  			</div>
				  		</div>
				  		
				  		<div class="span4 offset1">
					  		<div class="form-actions">
					  			<button type="submit" class="btn btn-primary">Login</button>
					  		</div>
				  		</div>
				  	</fieldset>
				</form>
                            
			</div>
		</div>
         
	</body>
</html>


