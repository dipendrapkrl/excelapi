
		 <?php
                
                
		 	require_once 'header.php';
                        
                         
		 ?>
		
             <script type="text/javascript" src="js/jquery.form.js"></script>
		
		<script type="text/javascript">
		
		
			$(document).ready(function() {
				
				// make it an "ajax form"
				$('#excelImportForm').ajaxForm({
					
					success: function(responseText, statusText, xhr, $form) {
								$("#resultDiv").html(responseText);
								$("#importProgress").removeClass("active");
								$("#importProgress").removeClass("progress-striped");
							}
					
				});
				
				// show modal dialog on submit
				$('#excelImportForm').submit(function() {
					$('#progressModal').modal('show');
				});
				
				
				
				
				
				$('#progressModal').on('hidden', function () {
					console.log("This is getting called.");
					var option = $("#countrySelect option:selected").val();
					var appendHTML = true;
					
					$("#resultDiv").html("");
					$("#importProgress").addClass("active");
					$("#importProgress").addClass("progress-striped");
				});
				
			});
		</script>
		
	 </head>
	<body data-spy="scroll" data-offset="50" data-twttr-rendered="true">
		
		<div class="container grid-bg" style="margin-top: 50px;">
			
			<div class="modal fade" id="progressModal">
				<div class="modal-header">
					<button class="close" data-dismiss="modal">×</button>
					<h3>Uploading...</h3>
				</div>
				<div class="modal-body">
					<div id="importProgress" class="progress progress-striped progress-success active">
						<div class="bar" style="width:100%"></div>
					</div>
					<div id="resultDiv"></div>
				</div>
				<div id="outputDiv" class="modal-footer">
					Depending on the size of your input, this might take a while.
				</div>
			</div>
			
			
			
			<div class="span4">
				<form class="form-horizontal" id="excelImportForm" action="getExcel" method="post" enctype="multipart/form-data">
				  	<fieldset>
				  		<legend>Upload an Excel file and enter relevant information </legend>
				    	
				    	<div class="control-group">
				    		<label class="control-label" for="dataFile">Choose File:</label>
				    		<div class="controls inline">
                                                    <input class="input-file" required="required" name="dataFile" id="dataFile" type="file" >
				    		</div>
				  		</div>
				  		
				  		
				  		
				  		<div class="control-group">
				  			<label class="control-label" for="adm4heading">Name Heading:</label>
				  			<div class="controls">
				  				<input class="input-xlarge" required="required" type="text" id="adm4heading" 
				  						placeholder= 'Enter value for Name.' name="nameheading">
				  			</div>
				  		</div>
				  		
				  		<div class="control-group">
				  			<label class="control-label" for="latitudeheading">Latitude Heading:</label>
				  			<div class="controls">
				  				<input class="input-xlarge" required="required" type="text" id="latitudeheading" 
				  						placeholder= "Enter value for Latitude or 'None'." name="latitudeheading">
				  			</div>
				  		</div>
				  		<div class="control-group">
				  			<label class="control-label" for="longitudeheading">Longitude Heading:</label>
				  			<div class="controls">
                                                            <input class="input-xlarge" required="required" type="text" id="longitudeheading" 
				  						placeholder= "Enter value for Longitude or 'None'." name="longitudeheading">
				  			</div>
				  		</div>
				  		<div class="control-group">
				  			<label class="control-label" for="addressheading">Address Heading:</label>
				  			<div class="controls">
                                                            <input class="input-xlarge" required="required" type="text" id="longitudeheading" 
				  						placeholder= "Enter value for Address or 'None'." name="addressheading">
				  			</div>
				  		</div>
                                                <div class="control-group">
				  			<label class="control-label" for="itunesheading">iTunes Link Heading:</label>
				  			<div class="controls">
                                                            <input class="input-xlarge" required="required" type="text" id="itunesheading" 
				  						placeholder= "Enter value for iTunes Link or 'None'." name="itunesheading">
				  			</div>
				  		</div>
                                                <div class="control-group">
				  			<label class="control-label" for="playstoreheading">Playstore Link Heading:</label>
				  			<div class="controls">
                                                            <input class="input-xlarge" required="required" type="text" id="playstoreheading" 
				  						placeholder= "Enter value for Playstore Link or 'None'." name="playstoreheading">
				  			</div>
				  		</div>
				  		
				  		<div class="span4 offset1">
					  		<div class="form-actions">
					  			<button type="submit" class="btn btn-primary">Import</button>
					  		</div>
				  		</div>
				  	</fieldset>
				</form>
                            
			</div>
		</div>
         
	</body>
</html>

