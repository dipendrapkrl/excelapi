<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Excel_to_array extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('PHPExcel/Classes/PHPExcel');

        ini_set("memory_limit", '-1');
    }

    public function index() {
        $headingArray = array(
            'adm1' => 'ADM1',
            'Province' => 'Province',
            'District' => 'District',
            'Ward' => 'Ward',
            'lat' => 'lat',
            'lng' => 'long'
        );
        $excelFilePath = 'files/dataVN.xlsx';
        $this-> get_array_data_from_excel($headingArray, $excelFilePath, TRUE, TRUE);
    }
/**A simple function that simply maps rows of excel to array
 * Generates array with provided headings
 * Dont forget to include PHPExcel to library in CI 
 * eg.  $this->load->library('PHPExcel/Classes/PHPExcel');
 * 
 * 
 * @param type $headingsArray is an array that is preferably in key value pair with value field pointing to the heading of the column
 *              eg. $headingArray = array(
            'adm1' => 'ADM1',
            'Province' => 'Province',
            'District' => 'District',
            'Ward' => 'Ward',
            'lat' => 'lat',
            'lng' => 'long'
        );
 * @param type $excelFilePath the exact path to the excel file location eg. 'files/data.xlsx'
 * @param type $includeNonSpecifiedColumns whether to include the columns that are not specified by the heading array
 * @param type $areAllRequired are all heading required to be checked before proceeding?
 * @return an array result
 * Generally if the particular field is not provided, its value will be set to empty 
 */
   
    public function get_array_data_from_excel($headingsArray, $excelFilePath, $includeNonSpecifiedColumns = TRUE, $areAllRequired = FALSE) {
        //headingArray should be in format key=>value pair

        $objReader = $this->getAppropriateReader($excelFilePath);
        if ($objReader == NULL) {
            echo "Unknown file type! Please Upload file with extension .xlsx i.e. only";
            die();
        }

        $objReader->setReadDataOnly(TRUE);
        $this->objPHPExcel = $objReader->load($excelFilePath);
        $activeSheet = $this->objPHPExcel->getSheet();
        $rowIterator = $activeSheet->getRowIterator();


        $array_data = array();
        $columnIndices = array();
        $areAllAvailable;
       

        foreach ($rowIterator as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);

            $rowIndex = $row->getRowIndex();
            foreach ($cellIterator as $cell) {

                if (1 === $row->getRowIndex()) {

                    $isCellUsed = FALSE;

                    $cellCalculatedValue = trim($cell->getCalculatedValue());
                    foreach ($headingsArray as $key => $value) {
                        if ($value == $cellCalculatedValue) {
                            $columnIndices[$key] = $cell->getColumn();
                            $isCellUsed = TRUE;
                            break;
                        }
                    }
                    if ($includeNonSpecifiedColumns) {

                        if (!$isCellUsed)
                           
                            $columnIndices[$cellCalculatedValue] = $cell->getColumn();
                           
                    }


                    continue;
                } else {
                    if ($areAllRequired) {
                        if (!isset($areAllAvailable))
                            $areAllAvailable = $this->areAllAvailable($headingsArray, $columnIndices);

                        if (!$areAllAvailable) {
                            echo "Sorry,Provided Headings were not found in the file";

                            die();
                        } else {

                            $columnIndices = $this->setColumnIndices($columnIndices, array_keys($headingsArray));
                        }
                    }
                }

                $columnIndex = $cell->getColumn();
                $cellValue = $cell->getCalculatedValue();

                foreach ($columnIndices as $key => $value) {
                    if ($columnIndex == $value) {
                        $array_data[$rowIndex][$key] = $cellValue;
                        break;
                    }
                }
            }
        }
        
        $finalArrayData= $array_data;
        foreach ($array_data as $key => $value) {
            foreach ($columnIndices as $key1 => $val1) {
                if(!isset($value[$key1])){
                    $finalArrayData[$key][$key1]='';
                }
            }
        }
        


        echo "<pre>";
        print_r($finalArrayData);
        return $finalArrayData;
    }

    private function setColumnIndices($columnIndices, $keys) {

        foreach ($keys as $value) {
            if (!isset($columnIndices[$value])) {
                $columnIndices[$value] = '';
            }
        }
        return $columnIndices;
    }

    private function areAllAvailable($headingsArray, $columnIndices) {


        $areAllAvailable;
        foreach ($headingsArray as $key => $value) {
            if (!isset($areAllAvailable))
                $areAllAvailable = isset($columnIndices[$key]);
            else
                $areAllAvailable = $areAllAvailable && isset($columnIndices[$key]);
        }

        return $areAllAvailable;
    }

    private function getAppropriateReader($pFilename) {
        $result;
        $ext = pathinfo($pFilename, PATHINFO_EXTENSION);
        switch ($ext) {
            case 'xlsx':
                $result = new PHPExcel_Reader_Excel2007();
                break;
            case 'xls':
                echo "only xlsx files are supported";
                unlink($pFilename);
                die();
                // $result = new PHPExcel_Reader_Excel5();
                break;

            case 'ods':
                echo "only xlsx files are supported";
                unlink($pFilename);
                die();
                // $result = new PHPExcel_Reader_OOCalc();
                break;

            default:
                echo "only xlsx files are supported";
                unlink($pFilename);
                die();
                //   $result = NULL;
                break;
        }
        return $result;
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */